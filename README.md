## About me

Hi, my name is Aleksandra Dziedzic and I am at the starting point of my career as a Software Tester. 


## Software Development Academy course

Currently I am in the middle of 105-hour-long course organised by Software Development Academy. Topics that will be covered:

**Test Design Techniques** | **Testing Classes** | **Programming Basics** | **Database Basics** | **SCRUM Workshop** | **BDD** | **Selenium WebDriver** | **Final Project**

It is a set of very practical lessons, where I am working on real-case scenarios - some examples you might find in _My projects_ paragraph. 

Also the course is a great preparation to ISTQB certification which I would like to take after finishing it.


## My projects
* Test cases for website [SauceDemo.com](https://www.saucedemo.com/)
* Test cases for website [Electro.pl](https://electro.pl)
* Test cases for website [Medicine](https://wearmedicine.com)


